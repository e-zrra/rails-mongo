class Employee
  include Mongoid::Document
  field :name, type: String
  #embedded_in :company
  belongs_to :company, :inverse_of => :employees
end
