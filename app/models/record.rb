class Record
  include Mongoid::Document
  field :date_input, type: Date
  field :time_input, type: Time
  field :output, type: Date
  field :output, type: Time
  field :input_output, type: Integer
  #embedded_in :visitor
  #embedded_in :company
  belongs_to :company
  belongs_to :visitor
end
