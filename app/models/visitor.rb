class Visitor
  include Mongoid::Document
  field :name, type: String
  field :last_name, type: String
  #embedded_in :company
  belongs_to :company
end
