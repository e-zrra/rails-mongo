class Agenda
  include Mongoid::Document
  field :date, type: Date
  field :time, type: Time
  #embedded_in :employee
  #embedded_in :company

  belongs_to :employee

  belongs_to :company
end
