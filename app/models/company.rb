class Company
  include Mongoid::Document
  field :name, type: String
  #embeds_one :employees
  has_many :employees
  belongs_to :role
end
